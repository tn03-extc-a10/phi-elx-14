#ifndef_breathanalysis
#def_breathanalysis

/*-------------------------------------------------------------------------------------------------
Function to take user id 
*/-------------------------------------------------------------------------------------------------
typedef struct UserId
 {
   Const Char UserId; 
   Const Char name; 
   int age;
   Const Char gender;
 };
(UserId*) get_User_id(const char)
Boolean authenticate_user_id;


/*-------------------------------------------------------------------------------------------------
Function to gather the sensor data
*/-------------------------------------------------------------------------------------------------
typedef enum {
updatesensordata;
readsensordata;
checklimit;
}sensordata;

get_sensor_data(float)
Boolean check_limit;


/*-------------------------------------------------------------------------------------------------
Function to initiate the alarm system
*/-------------------------------------------------------------------------------------------------

void alarm_system(sensordata);

#endif